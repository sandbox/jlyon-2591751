<a href="<?php print $url ?>" title="<?php print strip_tags($widget_title) ?>" class="addthisevent <?php print $class ?>">
    <?php print t($widget_title) ?>
    <span class="_start"><?php print $start ?></span>
    <span class="_end"><?php print $end ?></span>
    <span class="_zonecode"><?php print $zonecode ?></span>
    <span class="_summary"><?php print $summary ?></span>
    <span class="_description"><?php print $description ?></span>
    <span class="_location"><?php print $location ?></span>
    <span class="_all_day_event"><?php print $all_day ?></span>
    <span class="_date_format"><?php print $date_format ?></span>
</a>