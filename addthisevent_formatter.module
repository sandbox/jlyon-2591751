<?php



/******************************* FIELD FORMATTERS ***************************** */

/**
* Implements hook_field_formatter_info().
*/
function addthisevent_formatter_field_formatter_info() {
  $info = array(
    'addthisevent_formatter' => array(
      'label' => t('AddThisEvent dropdown'),
      'field types' => array('date', 'datestamp', 'datetime'),
      'settings'  => array(
        'link_class' => '',
        'text' => 'Add to calendar',
      ),
    ),
  );
  return $info;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function addthisevent_formatter_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  // Add fields
  switch ($display['type']) {
      
    case 'addthisevent_formatter':
      $element['text'] = array(
        '#title' => t('Button text'),
        '#type' => 'textfield',
        '#default_value' => $settings['text'],
      );
      $element['link_class'] = array(
        '#title' => t('Link class'),
        '#type' => 'textfield',
        '#default_value' => $settings['link_class'],
        '#description' => t('Enter a class to add to the <a> element.'),
      );
      break;
  }
  
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 * @todo
 */
function addthisevent_formatter_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = array();

  foreach ($settings as $key => $value) {
    $summary[] = $key . ': ' . $value;
  }

  return implode('<br />', $summary);
}


/**
 * Implements hook_field_formatter_view().
 */
function addthisevent_formatter_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];
    
  foreach ($items as $delta => $item) {
    
    switch ($display['type']) {
      
      case 'addthisevent_formatter':
        $tid = !empty($item['target_id']) ? $item['target_id'] : (!empty($item['tid']) ? $item['tid'] : NULL);
        $term = taxonomy_term_load($tid);
        $label = entity_label('taxonomy_term', $term);
        $options = array();
        if (!empty($settings['query'])) {
          $query = str_replace('[tid]', $tid, $settings['query']);
          parse_str($query, $options['query']);
        }
        if (!empty($settings['link_class'])) {
          $options['attributes']['class'][] = $settings['link_class'];
        }
        $element[$delta] = array(
          '#type' => 'markup',
          '#markup' => theme('addthisevent_formatter', array(
            'node' => $entity,
            'items' => $items,
            'class' => $settings['link_class'],
            'widget_title' => $settings['text'],
          )),
        );
        break;
    }

  } // foreach

  return $element;
}


/******************************* DISPLAY SUITE ***************************** */


/**
 * Field returns date in "custom time" format.
 */
function addthisevent_formatter_widget($field) {
  $date_language = field_language($field['entity_type'], $field['entity'], 'field_date');
  return theme('addthisevent_formatter', array(
    'node' => $field['entity'],
    'items' => $field['entity']->field_date[$date_language],
  ));
}


/**
 * Find the next upcoming event or the last event in a series.
 * From oa_events.module.
 *
 * @param  array $items
 *   An array of dates attached to a field.
 * @return array
 *   An array, keyed by delta and event.
 */
function _addthisevent_formatter_find_next_event($items) {
  // Set some base params we'll need.
  $now = time();
  $event = NULL;
  $event_delta = NULL;
  $next_event = NULL;
  $last_event = NULL;
  $next_event_delta = NULL;
  $last_event_delta = NULL;
  $diff = 0;

  // Loop through all events.
  foreach ($items as $delta => $date) {
    // If event is a future date.
    $date_value = strtotime($date['value']);
    if ($date_value >= $now) {
      // That happens before the last found future event
      if ($diff == 0 || ($date_value - $now) < $diff) {
        // Set it as the currently found next event, and update the difference.
        $diff = $date_value - $now;
        $next_event = $date;
        $next_event_delta = 0;
      }
    }
    // Determine if this is the last event in the series.
    else if (empty($last_event) || ($date['value'] > $last_event['value'])) {
      $last_event = $date;
      $last_event_delta = $delta;
    }
  }

  // If there is no future event we'll use the last occurrence.
  if (empty($next_event)) {
    $event = $last_event;
    $event_delta = $last_event_delta;
  }
  else {
    $event = $next_event;
    $event_delta = $next_event_delta;
  }

  return array(
    'delta' => $event_delta,
    'event' => $event,
  );
}


/******************************* THEME ***************************** */

/**
 * Implements hook_theme().
 */
function addthisevent_formatter_theme($existing, $type, $theme, $path) {
  $path = 'templates/';
  return array(
    'addthisevent_formatter' => array(
      'template' => $path . 'addthisevent',
      'variables' => array(
        'node' => array(),
        'date_format' => 'DD/MM/YYYY',
        'all_day' => 'false',
        'widget_title' => t('Add to calendar'),
        'class' => '',
      ),
    ),
  );
}


/**
 * Lightbox theme preprocess.
 */
function template_preprocess_addthisevent_formatter(&$variables) {
  $node = $variables['node'];

  // @todo
  $variables['url'] = url('node/'.$node->nid, array('absolute' => TRUE));
  $variables['summary'] = check_plain($node->title);
  $description_language = field_language('node', $node, 'body');
  $variables['description'] = !empty($node->body[$description_language][0]['safe_value']) ? strip_tags($node->body[$description_language][0]['safe_value']) : '';
  $location_language = field_language('node', $node, 'body');
  $variables['location'] = check_plain($node->field_location_description[$location_language][0]['value']);

  $event_info = _addthisevent_formatter_find_next_event($variables['items']);
  $date = $event_info['event'];
  //$date_field_date1 = new DateObject($date['value'], $date['timezone_db'], DATE_FORMAT_ISO);
  //$date_field_date2 = new DateObject($date['value2'], $date['timezone_db'], DATE_FORMAT_ISO);
  //$variables['start'] = date_format_date($date_field_date1, 'custom', 'm-d-Y H:i:s');
  //$variables['end'] = date_format_date($date_field_date2, 'custom', 'm-d-Y H:i:s');
  $variables['start'] = $date['value'];
  $variables['end'] = $date['value2'];
  $variables['zonecode'] = '35';//variable_get('addthisevent_formatter_zonecode', 35); //UTC @todo: make this editable on settings pg?

  $variables['all_day'] = date_is_all_day(date(DATE_FORMAT_DATETIME, $date['value']), date(DATE_FORMAT_DATETIME, $date['value2'])) ? 'true' : 'false';

  // Add the js
  //drupal_add_js(drupal_get_path('module', 'addthisevent_formatter') . '/js/ate-latest.min.js');
  drupal_add_js('https://addthisevent.com/libs/1.5.8/ate.min.js', array('external' => TRUE));
}